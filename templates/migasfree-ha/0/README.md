# Migasfree Server HA (experimental)

### Description

Provides an migasfree server in High Availability.

### Hosts requirements

* One host with a rancher label **role=database**

* In all other hosts:

    * Migasfree server needs a certain entropy in the host to generate gpg keys. If your host is based in Debian you should run:

        ```
            apt-get install haveged
        ```

    * Is necessary too install and config ipvsadm for virtual ip:

        ```
            apt-get install ipvsadm
        ```

    * You should configure ipvsadm to start ipvsadm on boot. Edit /etc/default/ipvsadm and set:

        ```
            AUTO="true"
        ```

    * Is required to use a network file system (glusterfs, nfs, etc.) to share data between all hosts. Mount a volume in the path especified by **DIR** (default /var/lib/migasfree).

        Sample mounting glusterfs volume in /etc/fstab:

        ```
            192.168.92.218:/migasfree /var/lib/migasfree/data glusterfs defaults,_netdev 0 0
        ```


### License

GPL 3.0

__NOTE__: All components in this stack are open source tools available in the community.
