# Migasfree Server (experimental in one host)

### Description

Provides an isolated migasfree server to run in one host.

This template deploys two containers:

* **migasfree/server** - It's the migasfree server. (http://github.com/migasfree/migasfree)
* **migasfree/db** - It's the SGBD (http://postgresql.org)

Once deployed you should have a Migasfree Server.

### Requirements

* One host with a rancher label **role=database**

* Migasfree server needs a certain entropy in the host to generate gpg keys. If your host is based in Debian you should run:

    ```
        apt-get install haveged
    ```

### Data persistence

You will have all data in the host directory: `/var/lib/migasfree`

### License

GPL 3.0

__NOTE__: All components in this stack are open source tools available in the community.
